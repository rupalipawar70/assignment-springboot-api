package com.intellectdesign.assignment.api.util;

import java.util.Date;

import org.springframework.stereotype.Service;

@Service
public class ValidatorUtil {

	public boolean validateUserName(String name) {
		return name.matches( "[a-zA-Z]*" );
	}
	
	public boolean validatePincode(Number pincode) {
		String pinCode = String.valueOf(pincode);
		String pinCodePattern = "\\d{6}(-\\d{4})?";
	    return pinCode.matches(pinCodePattern);
	}
	
	public boolean compateDates(Date inputDate) {
		Date currentDate = new Date();
        if(inputDate.after(currentDate)){
            return false;
        }
        if(inputDate.equals(currentDate)){
            return false;
        }
		return true;
	}
}
