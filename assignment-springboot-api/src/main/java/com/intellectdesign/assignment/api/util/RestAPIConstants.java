package com.intellectdesign.assignment.api.util;

public enum RestAPIConstants {
	
	USER_CONTROLLER_V1("/v1/users");
	
	private String constant;

	RestAPIConstants(String value) {
		this.constant = value;
	}
	
	public String getRestAPIConstants() {
		return constant;
	}
}
